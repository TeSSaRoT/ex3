package com.example.itryex3

import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.google.android.material.snackbar.Snackbar


private val projectionDisplayName: Array<String> = arrayOf(
    ContactsContract.Contacts.LOOKUP_KEY,
    ContactsContract.Contacts.DISPLAY_NAME
)

private val projectionPhone: Array<String> = arrayOf(
    ContactsContract.CommonDataKinds.Phone.NUMBER,
)

private val projectionEmail: Array<String> = arrayOf(
    ContactsContract.CommonDataKinds.Email.ADDRESS
)

private const val selectionPhone = "${ContactsContract.CommonDataKinds.Phone.LOOKUP_KEY} = ?"
private const val selectionEmail = "${ContactsContract.CommonDataKinds.Email.LOOKUP_KEY} = ?"
private val selectionArgs: Array<String> = arrayOf("")

@Database(entities = [Contact::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun contactDao(): ContactDao
}

private const val channelId = R.string.channelId.toString()

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createNotificationChannel()
        checkButtons()

        val selectButton: Button = findViewById(R.id.selectContactButton)
        val showContactButton: Button = findViewById(R.id.showContactButton)
        val showSPButton: Button = findViewById(R.id.showSPButton)
        val showNotificationButton: Button = findViewById(R.id.showNotificationButton)
        selectButton.setOnClickListener {
            openContactsActivityForResult()
        }
        showContactButton.setOnClickListener {
            showDialog()
        }
        showSPButton.setOnClickListener {
            showSnackBar(showSP())
        }
        showNotificationButton.setOnClickListener {
            showNotification()
        }
    }

    private fun checkButtons() {
        val selectButton: Button = findViewById(R.id.selectContactButton)
        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    android.Manifest.permission.READ_CONTACTS
                )
            ) {
                selectButton.isEnabled = false
                Toast.makeText(this, "Нужно разрешение на доступ к Контактам", Toast.LENGTH_SHORT)
                    .show()
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(android.Manifest.permission.READ_CONTACTS), 1618
                )
            }
        }
        showButtons()
    }

    private fun showButtons() {
        val showContactButton: Button = findViewById(R.id.showContactButton)
        val showSPButton: Button = findViewById(R.id.showSPButton)
        val showNotificationButton: Button = findViewById(R.id.showNotificationButton)
        if (showSP() != "nil") {
            showContactButton.isEnabled = true
            showSPButton.isEnabled = true
            showNotificationButton.isEnabled = true
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1618 -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() &&
                            grantResults[0] == PackageManager.PERMISSION_GRANTED)
                ) {
                    // Permission is granted. Continue the action or workflow
                    // in your app.
                } else {
                    val selectButton: Button = findViewById(R.id.selectContactButton)
                    selectButton.isEnabled = false
                }
                return
            }
            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    private fun openContactsActivityForResult() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.data = ContactsContract.Contacts.CONTENT_URI
        resultLauncher.launch(intent)
    }

    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent = result.data!!
                //doSomeOperations()
                var name = ""
                var phone = ""
                var email = ""
                var lookupKey: String? = null
                val contactURI: Uri? = data.data
                val cr = contentResolver

                val cursorDisplayName = cr.query(
                    contactURI!!,
                    projectionDisplayName, null, null, null
                )

                cursorDisplayName.use { cursor ->
                    cursor?.moveToFirst()
                    if (cursor != null) {
                        lookupKey = cursor.getString(0)
                        name = cursor.getString(1)
                    }
                }

                selectionArgs[0] = lookupKey!!

                val cursorPhone = contactURI.let {
                    cr.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        projectionPhone, selectionPhone, selectionArgs, null
                    )
                }
                cursorPhone.use { cursor ->
                    cursor?.moveToFirst()
                    if (cursor != null) {
                        phone = cursor.getString(0)
                    }
                }

                val cursorEmail = contactURI.let {
                    cr.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                        projectionEmail, selectionEmail, selectionArgs, null
                    )
                }
                cursorEmail.use { cursor ->
                    cursor?.moveToFirst()
                    if (cursor != null) {
                        email = cursor.getString(0)
                    }
                }

                val contactDao = getRoom().contactDao()
                contactDao.insertContact(copyContact(name, phone, email))
                Toast.makeText(this, "Сохранение успешно", Toast.LENGTH_SHORT).show()
                val showContactButton: Button = findViewById(R.id.showContactButton)
                showContactButton.isEnabled = true
            }
        }

    private fun getRoom(): AppDatabase {
        return Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "contacts_base"
        ).allowMainThreadQueries().build()
    }

    private fun copyContact(name: String, phone: String, email: String): Contact {
        return Contact(null, name, phone, email)
    }

    private fun showDialog() {
        val textName: TextView = findViewById(R.id.textView)
        val textEmail: TextView = findViewById(R.id.textView2)
        val contactDao = getRoom().contactDao()
        val items: Array<String> = contactDao.loadNumbers()
        println(items[0])
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.dialogTitle)
            .setItems(
                items
            ) { _, which ->
                textName.text = contactDao.getName(items[which])
                textEmail.text = contactDao.getEmail(items[which])
                saveInSP(items[which])
                showButtons()
            }
        builder.create()
        builder.show()
    }

    private fun saveInSP(number: String) {
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE) ?: return
        with(sharedPref.edit()) {
            putString(getString(R.string.numberSP), number)
            apply()
        }
        Toast.makeText(this, "Сохранение в SP успешно", Toast.LENGTH_SHORT).show()
    }

    private fun showSP(): String {
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE)
        val highScore = sharedPref.getString(
            getString(R.string.numberSP),
            resources.getString(R.string.numberSPDefault)
        )
        return highScore!!
    }

    private fun showSnackBar(msg: String) {
        val contextView = findViewById<View>(R.id.showSPButton)
        Snackbar.make(contextView, msg, Snackbar.LENGTH_SHORT)
            .show()
    }

    private fun showNotification() {
        val contactDao = getRoom().contactDao()

        val builder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.ic_baseline_people_alt_24)
            .setContentTitle(showSP())
            .setContentText(contactDao.getName(showSP()))
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)

        with(NotificationManagerCompat.from(this)) {
            notify(111, builder.build())
        }
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name)
            val descriptionText = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(channelId, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}