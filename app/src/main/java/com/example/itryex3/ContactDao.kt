package com.example.itryex3

import androidx.room.*

@Dao
interface ContactDao {
    @Query("SELECT * FROM Contact")
    fun getAll(): List<Contact>

    @Query("SELECT * FROM Contact WHERE uid IN (:contactIds)")
    fun loadAllByIds(contactIds: IntArray): List<Contact>

    @Query("SELECT * FROM contact WHERE full_name LIKE :full LIMIT 1")
    fun findByName(full: String): Contact

    @Query("SELECT number FROM Contact")
    fun loadNumbers(): Array<String>

    @Query("SELECT full_name FROM Contact WHERE number LIKE :num")
    fun getName(num: String): String

    @Query("SELECT email FROM Contact WHERE number LIKE :num")
    fun getEmail(num: String): String

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertContact(vararg contacts: Contact)

    @Delete
    fun delete(contact: Contact)
}