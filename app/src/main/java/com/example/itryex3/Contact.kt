package com.example.itryex3

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    indices = [Index(
        value = ["number"],
        unique = true
    )]
)
data class Contact(
    @PrimaryKey(autoGenerate = true) val uid: Int?,
    @ColumnInfo(name = "full_name") val fullName: String?,
    var number: String,
    val email: String?
)
